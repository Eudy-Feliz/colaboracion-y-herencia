﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

      
namespace Problema_3
    {
        public class A
        {
            public A()
            {
                Console.WriteLine("Constructor de la clase A");
            }
        }

        public class B : A
        {
            public B()
            {
                Console.WriteLine("Constructor de la clase B");
            }
        }

        public class C : B
        {
            public C()
            {
                Console.WriteLine("Constructor de la clase C");
            }
        }

        class Eudy_prueba
        {
            static void Main(string[] args)
            {
                C objeto = new C();
                Console.ReadKey();
            }
        }
    }
    


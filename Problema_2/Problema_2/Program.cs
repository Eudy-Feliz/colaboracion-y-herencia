﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_2
{

    public class contacto
    {
        private string _Nombre;
        private string _Apellido;
        private string _Telefono;
        private string _Direccion;

        public string Nombre
        {
            get
            {
                return _Nombre;
            }

            set
            {
                _Nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return _Apellido;
            }

            set
            {
                _Apellido = value;
            }
        }

        public string Telefono
        {
            get
            {
                return _Telefono;
            }

            set
            {
                _Telefono = value;
            }
        }

        public string Direccion
        {
            get
            {
                return _Direccion;
            }

            set
            {
                _Direccion = value;
            }
        }

        public void setcontacto(string nombre, string apellido, string telefono, string direccion)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Telefono = telefono;
            this.Direccion = direccion;
        }

        public void saludar()
        {
            string mensaje = "Hola, soy " + Nombre + " " + Apellido + " " + " mi numero de telefono es: " + Telefono + " " + " vivo en " + Direccion;

            Console.WriteLine(mensaje);


        }

    }
    public class ProbarContacto
    {
        static void Main(string[] args)
        {
            contacto obj_contacto1 = new contacto();
            obj_contacto1.setcontacto("Eudy", "Feliz", "829-663-4051", "Haina");

            contacto obj_contacto2 = new contacto();
            obj_contacto2.setcontacto("Miladis", "Feliz", "809-000-6350", "Haina");

            obj_contacto2.saludar();
            obj_contacto1.saludar();
            Console.ReadKey();
        }
    }

}
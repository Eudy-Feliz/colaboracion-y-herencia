﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problema_1
{
    class persona
    {
        private string Cedula;

        public string cedula
        {
            get
            {
                return cedula;

            }
            set
            {
                cedula = value;
            }
        }


        private string Nombre;

        public string nombre
        {
            get
            {
                return nombre;

            }
            set
            {
                nombre = value;
            }
        }


        private string Apellido;

        public string apellido
        {
            get
            {
                return apellido;

            }
            set
            {
                apellido = value;
            }
        }


        private int Edad;

        public string edad
        {
            get
            {
                return edad;

            }
            set
            {
                edad = value;
            }
        }

        public void imprimir()
        {
            Console.WriteLine("Cedula:" + Cedula);
            Console.WriteLine("Nombre:" + Nombre);
            Console.WriteLine("Apellido:" + Apellido);
            Console.WriteLine("Edad:" + Edad);
        }


        class profesor : persona
        {
            private float sueldo;

            public float Sueldo
            {
                set
                {
                    sueldo = value;
                }
                get
                {
                    return sueldo;
                }
            }

            new public void Imprimir()
            {
                base.imprimir();
                Console.WriteLine("Sueldo:" + Sueldo);
            }

        }


        static void Main(string[] args)
        {

            persona persona = new persona();
            persona.Cedula = "4024179682-6";
            persona.Nombre = "Eudy";
            persona.Apellido = "Feliz";
            persona.Edad = 20;
            Console.WriteLine("Los datos de la persona son:");
            persona.imprimir();


            profesor profesor = new profesor();
            profesor.Cedula = "4024179683-0";
            profesor.Nombre = "Miguel";
            profesor.Apellido = "Moreta";
            profesor.Edad = 40;
            profesor.Sueldo = 75000;
            Console.WriteLine("Los datos del profesor son:");
            profesor.Imprimir();


            Console.ReadKey();

        }
    }
}
